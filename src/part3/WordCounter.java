package part3;

import java.util.HashMap;

public class WordCounter {
	private String message;
	public HashMap<String, Integer> wordCount;

	public WordCounter(String message) {
		this.message = message;
		wordCount = new HashMap<String, Integer>();
	}
	
	

	public void count() {
		String s[] = message.split(" ");
		for (int i = 0; i < s.length; i++) {
			if(wordCount.keySet().contains(s[i])){
				int a = wordCount.get(s[i])+1;
				wordCount.replace(s[i], a);
			}else{
				wordCount.put(s[i], 1);
			}
		}
	}

	public Integer hashWord(String word) {
		if(wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		return 0; 
	}

}
