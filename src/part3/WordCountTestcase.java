package part3;

public class WordCountTestcase {

	public static void main(String[] args) {
		WordCounter counter = new WordCounter("here is the root of the root"); 
		counter.count();
	
		System.out.println(counter.hashWord("root"));
		System.out.println(counter.hashWord("is"));
		System.out.println(counter.hashWord("r"));
	}

}
