package part5;

public class Main {

	public static void main(String[] args) {
		Refrigerator ref = new Refrigerator();
		ref.put("a");
		ref.put("b");
		ref.put("c");
		System.out.println(ref.toString());
		ref.put("d");
		ref.put("e");
		System.out.println(ref.toString());
		
		ref.put("f");
		ref.put("G");
		
		System.out.println(ref.takeOut("a"));
		System.out.println(ref.takeOut("b"));
		System.out.println(ref.takeOut("k"));
		System.out.println(ref.toString());
	}

}
