package part5;

public class FullException extends Exception{

	public FullException(String string) {
		super(string);
	}
	
	public FullException(){
		super();
	}

}
