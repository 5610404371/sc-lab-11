package part5;

import java.util.ArrayList;

public class Refrigerator {
	private int size;
	private ArrayList<String> things = new ArrayList<String>();;
	
	public void put(String stuff){
		try{
			
			if(size>=5){
				throw new FullException();
			}
			things.add(stuff);
			size+=1;
			
		}catch(FullException e){
			System.err.println("Refrigerator's already full!");
		}
		
	}
	
	public String takeOut(String stuff){
		for(int i =0;i<things.size();i++){
			if(things.get(i).equals(stuff)){
				things.remove(i);
				return stuff;
			}
		}
		return null;
		
	}
	
	public String toString(){
		return things.toString();
	}

}
